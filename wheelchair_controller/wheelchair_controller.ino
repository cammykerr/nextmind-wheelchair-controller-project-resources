// defines pins numbers for ultrasonic sensor
#define trigPin 2
#define echoPin 3

// Motor A connections
#define enA 6
#define in1 7
#define in2 8

// Motor B connections
#define enB 11
#define in3 9
#define in4 10

char data = '0';

// declare variables for ultrasonic sensor
long duration;
int distance;

// the setup function runs once when you press reset or power the board
void setup() 
{
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600); // Starts the serial communication

  // Set all the motor control pins to outputs
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  
  // Turn off motors - Initial state
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);

  // Bluetooth Serial Communication
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);

  analogWrite(enA, 150);
  analogWrite(enB, 150);
}

// the loop function runs over and over again forever
void loop() 
{
  if(Serial.available() > 0)
  {
    data = Serial.read();

    if(data == '1')
    {
      digitalWrite(LED_BUILTIN, HIGH);
      stop_m();
    }
    else if(data == '2')
    {
      distance = get_distance();
      if(distance > 20 || distance == 0)
      {
        go_forward();
      }
      else
      {
        stop_m();
      }
    }
    else if(data == '3')
    {
      go_left();
    }
    else if(data == '4')
    {
      go_right();
    }
    else
    {
      stop_m(); 
    }
  }
  else
  {
    stop_m();
  }
}

void stop_m()
{
  // Turn off motors
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}

void go_right()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void go_forward()
{
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void go_left()
{
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);  
}

int get_distance()
{
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  
  // Calculating the distance
  return duration*0.034/2;
}
