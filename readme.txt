1. wheelchair controller
Contains the source code that runs on the Arduino car

2. wheelchair project
This is the application that enables the control of the car using the NextMind
brain-computer interface. Line 20 in the script Assets/bluetooth_signal_controller.cs
specifies the port number the Arduino is connected to.

4. models
This folder contains the 3d models and vector files that were 3d printed and laser cut 
to form the chassis of the Arduino car