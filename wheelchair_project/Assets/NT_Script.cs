﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NextMind.NeuroTags;


public class NT_Script : MonoBehaviour
{
    public NeuroTag neuroTag;

    public GameObject trigBox;

    // Start is called before the first frame update
    void Start()
    {
        trigBox.SetActive(false);

        neuroTag = GetComponent<NeuroTag>();
        neuroTag.onTriggered.AddListener(OnTriggered);
        neuroTag.onReleased.AddListener(OnReleased);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggered()
    {
        trigBox.SetActive(true);
    }

    void OnReleased()
    {
        trigBox.SetActive(false);
    }

}
