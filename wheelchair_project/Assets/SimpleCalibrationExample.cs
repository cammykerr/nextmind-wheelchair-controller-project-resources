﻿using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using NextMind.Calibration;
using NextMind;
using NextMind.Devices;

public class SimpleCalibrationExample : MonoBehaviour
{
    [SerializeField]
    private CalibrationManager calibrationManager;

    [SerializeField]
    private Text resultsText;

    public GameObject UI_Tag;
    public GameObject UI_Tag1;
    public GameObject UI_Tag2;

    public GameObject UI_Tag_Forward;
    public GameObject UI_Tag_Left;
    public GameObject UI_Tag_Right;

    public GameObject brain;

    // Start is called before the first frame update
    void Start()
    {
        UI_Tag_Forward.SetActive(false);
        UI_Tag_Left.SetActive(false);
        UI_Tag_Right.SetActive(false);

        brain.SetActive(false);

        StartCoroutine(StartCalibrationWhenReady());
    }

    private IEnumerator StartCalibrationWhenReady()
    {
        // Waiting for the NeuroManager to be ready
        yield return new WaitUntil(NeuroManager.Instance.IsReady);

        // Actually start the calibration process.
        calibrationManager.StartCalibration();

        // Listen to the incoming results
        calibrationManager.onCalibrationResultsAvailable.AddListener(OnReceivedResults);
    }

    private void OnReceivedResults(Device device, CalibrationResults.CalibrationGrade grade)
    {
        // Display the calibration grade to the user
        resultsText.text = $"Received results for {device.Name} with a grade of {grade}";

        // Destroy the calibration UI Tags
        Destroy(UI_Tag);
        Destroy(UI_Tag1);
        Destroy(UI_Tag2);

        // After three seconds destroy the results text
        Thread.Sleep(5000);
        Destroy(resultsText);

        // Set active the control tags
        UI_Tag_Forward.SetActive(true);
        UI_Tag_Left.SetActive(true);
        UI_Tag_Right.SetActive(true);

        brain.SetActive(true);
    }
}