﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO.Ports;
using System.Threading;

public class bluetooth_signal_controller : MonoBehaviour
{

    public GameObject forward;
    public GameObject left;
    public GameObject right;

    private SerialPort port;

    // Start is called before the first frame update
    void Start()
    {
        port = new SerialPort("COM7", 9600);
    }

    // Update is called once per frame
    void Update()
    {
        if(forward.activeSelf)
        {
            port.Open();
            port.Write("2");
            port.Close();
        }
        else if(left.activeSelf)
        {
            port.Open();
            port.Write("3");
            port.Close();
        }
        else if(right.activeSelf)
        {
            port.Open();
            port.Write("4");
            port.Close();
        }
        else
        {
            port.Open();
            port.Write("1");
            port.Close();
        }
    }
}
